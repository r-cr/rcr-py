__rcr() 
{
    local cur prev opts
    COMPREPLY=
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts="$(${COMP_WORDS[@]} --rcr-list)"

    if [ "${opts}" = "${prev}" ] ; then
        opts=
    fi

    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
}

complete -F __rcr rcr

#!/bin/sh

INSTALL="${INSTALL:-"cp -pRf"}"
MKDIR="${MKDIR:-"mkdir -p"}"
DESTDIR="${DESTDIR:-${HOME}/rrr}"
PREFIX="${PREFIX:-}"
P="${P:-"rcr"}"
BINDIR="${BINDIR:-${PREFIX}/bin}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
RCRDIR="${PREFIX}/libexec/rcr"
ETCDIR="/etc"

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_devdoc()
{
    ${MKDIR} "${DESTDIR}${RCRDIR}/"
    ${INSTALL} 'devdoc/' "${DESTDIR}${RCRDIR}/"
    chmod 555 "${DESTDIR}${RCRDIR}/devdoc/devdoc"
    chmod 555 "${DESTDIR}${RCRDIR}/devdoc/genlatex"
    chmod 555 "${DESTDIR}${RCRDIR}/devdoc/gendocbook"
}

install_bin()
{
    ${MKDIR} "${DESTDIR}${BINDIR}/"
    ${INSTALL} 'bin/rcr' "${DESTDIR}${BINDIR}/"
    chmod 555 "${DESTDIR}${BINDIR}/rcr"
}

install_doc()
{
    ${MKDIR} "${DESTDIR}${DOCDIR}/"
    ${INSTALL} \
        'README-rcr' \
        'License-ISC' \
        'doc/gen/rcr-howto.html' \
        "${DESTDIR}${DOCDIR}/"
}

install_etc()
{
    ${MKDIR} "${DESTDIR}/"
    ${INSTALL} 'etc/' "${DESTDIR}/"
}

install_helpers()
{
    ${MKDIR} "${DESTDIR}${RCRDIR}/"
    ${INSTALL} 'exec/help' "${DESTDIR}${RCRDIR}/"
    ${INSTALL} 'exec/man' "${DESTDIR}${RCRDIR}/"
    chmod 555 "${DESTDIR}${RCRDIR}/help"
    chmod 555 "${DESTDIR}${RCRDIR}/man"
}

set -o errexit
set -o xtrace

if [ "$1" = 'devdoc' ] ; then
    install_devdoc
    exit $?
fi

install_bin
install_doc
install_etc
install_helpers

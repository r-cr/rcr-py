$manMacro1a{'SelfProg'} = '\fB'; 
    $manMacro1b{'SelfProg'} = '\fP'; 
$htmlMacro1a{'SelfProg'} = '<span class="selfprogname">'; 
    $htmlMacro1b{'SelfProg'} = '</span>'; 
$manMacro1a{'SelfDesc'} = '\- ';
    $manMacro1b{'SelfDesc'} = '';
$htmlMacro1a{'SelfDesc'} = '&ndash; ';
    $htmlMacro1b{'SelfDesc'} = '';
$manMacro1a{'Envar'} = '\fB';
    $manMacro1b{'Envar'} = '\fP';
$htmlMacro1a{'Envar'} = '<span class="variable">';
    $htmlMacro1b{'Envar'} = '</span>';




$manMacro1a{'Prog'} = '\fB'; 
    $manMacro1b{'Prog'} = '\fP'; 

$manMacro1a{'File'} = '\fI';
    $manMacro1b{'File'} = '\fP';

$manMacro{'medbreak'}	= '\n';

$manMacro2a{'OptArg'} 		= '\fB';
    $manMacro2b{'OptArg'} 	= '\fP \fI';
    $manMacro2c{'OptArg'} 	= '\fP';
$manMacro2a{'OptoArg'} 		= '\fB';
    $manMacro2b{'OptoArg'} 	= '\fP [\fI';
    $manMacro2c{'OptoArg'} 	= '\fP]';
$manMacro2a{'oOptArg'} 		= '[\fB';
    $manMacro2b{'oOptArg'} 	= '\fP \fI';
    $manMacro2c{'oOptArg'} 	= '\fP]';
$manMacro2a{'oOptoArg'} 	= '[\fB';
    $manMacro2b{'oOptoArg'}	= '\fP [\fI';
    $manMacro2c{'oOptoArg'}	= '\fP]]';

$htmlMacro2a{'OptArg'} 		  = '<span class="optstyle">';
    $htmlMacro2b{'OptArg'} 	  = '</span> <span class="argstyle">';
    $htmlMacro2c{'OptArg'} 	  = '</span>';
$htmlMacro2a{'OptoArg'} 	  = '<span class="optstyle">';
    $htmlMacro2b{'OptoArg'}	  = '</span> [<span class="argstyle">';
    $htmlMacro2c{'OptoArg'}	  = '</span>]';
$htmlMacro2a{'oOptArg'} 	  = '[<span class="optstyle">';
    $htmlMacro2b{'oOptArg'}   = '</span> <span class="argstyle">';
    $htmlMacro2c{'oOptArg'}   = '</span>]';
$htmlMacro2a{'oOptoArg'} 	  = '[<span class="optstyle">';
    $htmlMacro2b{'oOptoArg'}  = '</span> [<span class="argstyle">';
    $htmlMacro2c{'oOptoArg'}  = '</span>]]';

$htmlMacro{'medbreak'}	= '<br/><br/>\n';

  $htmlMacro2a{'section'}   	   = '\n</div><!-- sectiontext -->\n\n<h2 class="sectionname"><a name="section_';
	  $htmlMacro2b{'section'}    = '">';
	  $htmlMacro2c{'section'}    = '</a></h2>\n\n<div class="sectiontext">\n';

  $htmlMacro2a{'sectionfirst'}   	   = '\n\n<h2 class="sectionname"><a name="section_';
	  $htmlMacro2b{'sectionfirst'}    = '">';
	  $htmlMacro2c{'sectionfirst'}    = '</a></h2>\n\n<div class="sectiontext">\n';

# replacement of html <font> with <span>, no other changes

  $htmlMacro1a{'Email'}		= '\n<span class="emailstyle">';
	  $htmlMacro1b{'Email'}	= '</span>';
  $htmlMacro1a{'URL'}		= '\n<span class="urlstyle">';
	  $htmlMacro1b{'URL'}	= '</span>';

  $htmlMacro1a{'Prog'} 		= '<span class="progname">';
	  $htmlMacro1b{'Prog'} 	= '</span>';
  $htmlMacro1a{'File'} 		= '<span class="filename">';
	  $htmlMacro1b{'File'} 	= '</span>';
  $htmlMacro1a{'Opt'} 		= '<span class="optstyle">';
	  $htmlMacro1b{'Opt'} 	= '</span>';
  $htmlMacro1a{'oOpt'} 		= '[<span class="optstyle">';
	  $htmlMacro1b{'oOpt'} 	= '</span>]';
  $htmlMacro1a{'Arg'} 		= '<span class="argstyle">';
	  $htmlMacro1b{'Arg'} 	= '</span>';
  $htmlMacro1a{'oArg'} 		= '[<span class="argstyle">';
	  $htmlMacro1b{'oArg'}    = '</span>]';

  $htmlMacro2a{'Cmd'} 		  = '<span class="commandname">';
	  $htmlMacro2b{'Cmd'} 	  = '</span>(';
	  $htmlMacro2c{'Cmd'} 	  = ')';
  $htmlMacro2a{'OptArg'} 		  = '<span class="optstyle">';
	  $htmlMacro2b{'OptArg'} 	  = '</span><span class="argstyle">';
	  $htmlMacro2c{'OptArg'} 	  = '</span>';
  $htmlMacro2a{'OptoArg'} 	  = '<span class="optstyle">';
	  $htmlMacro2b{'OptoArg'}	  = '</span>[<span class="argstyle">';
	  $htmlMacro2c{'OptoArg'}	  = '</span>]';
  $htmlMacro2a{'oOptArg'} 	  = '[<span class="optstyle">';
	  $htmlMacro2b{'oOptArg'}   = '</span><span class="argstyle">';
	  $htmlMacro2c{'oOptArg'}   = '</span>]';
  $htmlMacro2a{'oOptoArg'} 	  = '[<span class="optstyle">';
	  $htmlMacro2b{'oOptoArg'}  = '</span>[<span class="argstyle">';
	  $htmlMacro2c{'oOptoArg'}  = '</span>]]';

sub htmlItemStart
{
    NL; Print "<dl compact class='itemize' >"; NL;
}
sub htmlItemEnd
{
    NL; Print "</dd>\n</dl>"; NL;
}
sub htmlItemWithArg
{
    my $arg = $_[0];
    NL;
    if ($item_nr[$list_nest] > 1) {
	NL; Print "</dd>"; NL;
    }
    my $tag_style = '';
    if ($cur_list[$list_nest] eq 'descr')
    {
        $tag_style = ' class="description" ';
    }
    elsif ($cur_list[$list_nest] eq 'item')
    {
        $tag_style = ' class="itemize" ';
    }
    Print "<dt" . $tag_style . ">";
    interpret_word $arg; Print "</dt>"; NL;
    Print "<dd" . $tag_style . ">";
}

sub htmlSection
{
    my ($cnt, $kind, $section) = @_;
    if ($cnt == 1)
    {
        $kind = $kind . 'first' ;
    }
    interpret_line "\\$kind\{$cnt\}\{$section\}";
}

sub htmlNameEnd
{
    Print '</div><!-- sectiontext -->\n\n';
    Print '\n@@INSERTION-POINT@@-TOC@@\n';
}

sub manItemWithArg
{
    my $arg = $_[0];
    if ($manRS == 1) {
	Print '\n.RE\n';
    }
    $manRS      = 0;

    my $macro_item = '';
    my $argword = 1;
    my $spaceword = 1;
    if ($arg eq '')
    {
        $macro_item = '.HP';
    }
    elsif ($cur_list[$list_nest] eq 'descr')
    {
        $macro_item = '.TP';
        $spaceword = 0;
    }
    elsif ($cur_list[$list_nest] eq 'item')
    {
        $macro_item = '.IP ' . $arg;
        $argword = 0;
        $spaceword = 0;
    }

    Print '\n' . $macro_item . '\n';
    if ($argword == 1)
    {
        interpret_word $arg;
    }
    if ($spaceword == 1)
    {
        PrintM ' ';
    }
    NL;
}
sub manItem
{
    if ($manRS == 1) {
	Print '\n.RE\n';
    }
    $manRS      = 0;
    if ($cur_list[$list_nest] eq 'item')
    {
        Print '\n.HP\n';
    } elsif ($cur_list[$list_nest] eq 'enum')
    {
        Print '\n.TP\n';
	    Print $item_nr[$list_nest] . '.';
    }
    NL;
}
